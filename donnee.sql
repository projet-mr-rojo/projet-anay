insert into personnel values(1,'andy','0000','serveur');
insert into personnel values(NULL,'kiady','0000','serveur');
insert into personnel values(NULL,'admin','admin','admin');

insert into plats values(01,2000,01,'Riz','riz.jpeg');
insert into plats values(02,15000,01,'Soupe','Soupe.jpeg');
insert into plats values(NULL,10000,02,'Sandwich','Sandwich.jpeg');
insert into plats values(NULL,15000,01,'Mine-sao','Mine-sao.jpeg');

insert into categorie_plat values(1,'Plats');
insert into categorie_plat values(2,'Snacks');
insert into categorie_plat values(3,'Boisson');
