<?php 
   class Menu_controller extends CI_Controller  {
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
      }
     
      function menu(){
         $this->load->model('Plats')
         $idCategorie=$this->input->('categorie');         
         $plat_du_jour['plats']=$this->Plats->GetPlatWithId($idCategorie);

         $this->load->view('accueil.php',$data);

      }
   }