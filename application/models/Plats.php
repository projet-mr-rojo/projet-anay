<?php
    class Plats extends CI_Controller {
        private $idPlats;
        private $prix;
        private $idCategorie;
        private $nomPlats;
        private $photo;

        /**
         * Get the value of idPlats
         */ 
        public function getIdPlats()
        {
                return $this->idPlats;
        }

        /**
         * Set the value of idPlats
         *
         * @return  self
         */ 
        public function setIdPlats($idPlats)
        {
                $this->idPlats = $idPlats;

                return $this;
        }

        /**
         * Get the value of prix
         */ 
        public function getPrix()
        {
                return $this->prix;
        }

        /**
         * Set the value of prix
         *
         * @return  self
         */ 
        public function setPrix($prix)
        {
                $this->prix = $prix;

                return $this;
        }

        /**
         * Get the value of nomPlats
         */ 
        public function getNomPlats()
        {
                return $this->nomPlats;
        }

        /**
         * Set the value of nomPlats
         *
         * @return  self
         */ 
        public function setNomPlats($nomPlats)
        {
                $this->nomPlats = $nomPlats;

                return $this;
        }

        /**
         * Get the value of photo
         */ 
        public function getPhoto()
        {
                return $this->photo;
        }

        /**
         * Set the value of photo
         *
         * @return  self
         */ 
        public function setPhoto($photo)
        {
                $this->photo = $photo;

                return $this;
        }

        /**
         * Get the value of idCategorie
         */ 
        public function getIdCategorie()
        {
                return $this->idCategorie;
        }

        /**
         * Set the value of idCategorie
         *
         * @return  self
         */ 
        public function setIdCategorie($idCategorie)
        {
                $this->idCategorie = $idCategorie;

                return $this;
        }

        public function GetPlatWithId($idCategorie){
            $result=array();
            $count=0;
            $query=$this->db->get_where('plats', array('idcategorie' => $idCategorie);
            $resultat=$query->result_array();
            return $resultat;    
            }
        }
    }
?>