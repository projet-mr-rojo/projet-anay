<?php
    class Categorie_Plats extends CI_Controller{
        private $idCategorie;
        private $liste_plats;
        private $nom_categorie;
        

        /**
         * Get the value of idCategorie
         */ 
        public function getIdCategorie()
        {
                return $this->idCategorie;
        }

        /**
         * Set the value of idCategorie
         *
         * @return  self
         */ 
        public function setIdCategorie($idCategorie)
        {
                $this->idCategorie = $idCategorie;

                return $this;
        }

        /**
         * Get the value of liste_plats
         */ 
        public function getListe_plats()
        {
                return $this->liste_plats;
        }

        /**
         * Set the value of liste_plats
         *
         * @return  self
         */ 
        public function setListe_plats($liste_plats)
        {
                $this->liste_plats = $liste_plats;

                return $this;
        }

        /**
         * Get the value of nom_categorie
         */ 
        public function getNom_categorie()
        {
                return $this->nom_categorie;
        }

        /**
         * Set the value of nom_categorie
         *
         * @return  self
         */ 
        public function setNom_categorie($nom_categorie)
        {
                $this->nom_categorie = $nom_categorie;

                return $this;
        }
    }
?>