<?php 
class Personnel extends CI_Model{

    public $idPerso;
    public $log;
    public $mdp;
    public $profil;

    public function getIdPerso()
    {
        return $this->idPerso;
    }

    public function setIdPerso($idperso)
    {
        $this->idPerso = $idperso;

        return $this;
    }

    public function getLog()
    {
        return $this->log;
    }

    public function setNom($log)
    {
        $this->log = $log;

        return $this;
    }


    public function getMdp()
    {
        return $this->mdp;
    }

    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    public function getProfil()
    {
        return $this->profil;
    }

    public function setProfil($profil)
    {
        $this->profil = $profil;

        return $this;
    }
 
    public function check($username,$mdp){
        $resultat=array();
            $count=0;
            $query=$this->db->get_where('Personnel',array('log'=>$username));
            foreach($query->result() as $Personnel){
                    $count+=1;
                    $found=false;
                    if(strcmp($Personnel->log,$username)==0 && strcmp($mdp,$Personnel->mdp==0)){
                            $found=true;
                    }
                    if(!$found){
                        $resultat['errorPassword']=true;
                    }
                    else{
                        $resultat['Personnel']=array(
                                'idPerso'=>$Personnel->idPerso,
                                'profil'=>$Personnel->profil
                        );
                    }
            }
            if($count==0){
                    $resultat['errorLogin']=true;
            }
            return $resultat;
    }
    
}
?>

