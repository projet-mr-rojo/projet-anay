create table personnel(
    idPerso int Auto_increment Primary key,
    log VARCHAR(20),
    mdp VARCHAR(20),
    profil VARCHAR(10)
)ENGINE=InnoDB;

create table categorie_plat(
    idcategorie int Auto_increment Primary key,
    nom_categorie VARCHAR(30)
);

create table plats(
    idplats int Auto_increment Primary key,
    prix INT,
    idcategorie int,
    nom VARCHAR(100),
    photo VARCHAR(100)
)ENGINE=InnoDB;

create table commandes(
    idcommande int Auto_increment Primary key,
    liste_sakafo VARCHAR(20),
    daty date,
    num_table VARCHAR(10)
);

create table facture(
    idfacture int Auto_increment Primary key,
    liste_commande VARCHAR(20),
    prix_total int
);


